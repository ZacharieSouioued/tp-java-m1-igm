package fr.umlv.thread;

import java.util.ArrayList;
import java.util.List;

public class HelloThreadBug {

	public static void main(String[] args) throws InterruptedException {

		int threadCount = 4;
		int limit = 5000;

		List<Thread> threads = new ArrayList<>();
		ThreadSafeList<Integer> list = new ThreadSafeList<>();

		Runnable runnable = () -> {
			for (int i = 0; i < limit; i++) {
				list.safeAdd(i);
			}
		};

		for (int i = 0; i < threadCount; i++) {
			var thread = new Thread(runnable, "hello " + i);
			threads.add(thread);
			thread.start();
		}

		for(var thread: threads){
			thread.join();
		}
		System.out.println("taille final = " + list.size());
	}
}
