package fr.umlv.thread;

public class HelloThread {

	public static void main(String[] args) {
		Runnable runnable = () -> {
			for (int i = 0; i < 5000; i++) {
				System.out.println(Thread.currentThread().getName() + " " + i);
			}
		};

		int threadCount = 4;
		for (int i = 0; i < threadCount; i++) {
			var thread = new Thread(runnable, "hello " + i);
			thread.start();
		}
	}
}
