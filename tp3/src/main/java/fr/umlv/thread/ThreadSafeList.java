package fr.umlv.thread;

import java.util.ArrayList;

public class ThreadSafeList <E> {
	private final static Object obj = new Object();
	private final ArrayList<E> safeList = new ArrayList<>();

	public ThreadSafeList() {

	}

	public void safeAdd(E item) {
		synchronized(obj) {
			safeList.add(item);
		}
	}

	public int size() {
		return safeList.size();
	}

}