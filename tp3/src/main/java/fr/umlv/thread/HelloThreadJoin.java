package fr.umlv.thread;

import java.util.ArrayList;
import java.util.List;

public class HelloThreadJoin {

	public static void main(String[] args) throws InterruptedException {

		Runnable runnable = () -> {
			for (int i = 0; i < 5000; i++) {
				System.out.println(Thread.currentThread().getName() + " " + i);
			}
		};

		List<Thread> threads = new ArrayList<>();
		int threadCount = 4;
		for (int i = 0; i < threadCount; i++) {
			var thread = new Thread(runnable, "hello " + i);
			threads.add(thread);
			thread.start();
		}

		for(var thread: threads){
			thread.join();
		}
		System.out.println("Le programme est fini");
	}
}
