package fr.umlv.rental;

import lombok.NonNull;

public record Car(@NonNull String model, int year) implements Rentable{

    @Override
    public int insuranceCostAt(int year) {
        if(year < this.year){
            throw new IllegalArgumentException();
        }
        return year - this.year < 10 ? 200 : 500;
    }

    @Override
    public String toString() {
        return model + " " + year;
    }
}
