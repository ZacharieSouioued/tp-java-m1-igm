package fr.umlv.rental;

import lombok.NonNull;

public record Camel(@NonNull int year) implements Rentable{

    @Override
    public int insuranceCostAt(int year) {
        if(year < this.year){
            throw new IllegalArgumentException();
        }
        return (year - this.year) * 100;
    }

    @Override
    public String toString() {
        return "camel " + year;
    }
}
