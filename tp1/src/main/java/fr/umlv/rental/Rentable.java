package fr.umlv.rental;

public sealed interface Rentable permits Camel, Car {
    int year();
    int insuranceCostAt(int year);
}
