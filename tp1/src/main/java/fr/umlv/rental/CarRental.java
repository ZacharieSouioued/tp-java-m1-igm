package fr.umlv.rental;

import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class CarRental {

	private final ArrayList<Rentable> rentables = new ArrayList<>();

	public void add(@NonNull Rentable rentable) {
		rentables.add(rentable);
	}

	public void remove(@NonNull Rentable rentable) {
		if (!rentables.remove(rentable)) {
			throw new IllegalStateException();
		}
	}

	public List<Rentable> findAllByYear(int year) {
		return rentables.stream().filter(rentable -> rentable.year() == year).collect(Collectors.toList());
	}

	public int insuranceCostAt(int year) {
		return rentables.stream()
				.map(rentable -> rentable.insuranceCostAt(year))
				.reduce(Integer::sum)
				.orElse(0);
	}


	public Optional<Car> findACarByModel(@NonNull String model) {
		var iterator = rentables.iterator();
		if(!iterator.hasNext()) {
			return Optional.empty();
		}
		for(Rentable rentable = iterator.next(); iterator.hasNext(); rentable = iterator.next()) {
			switch (rentable) {
				case Car car -> {
					if(car.model().equals(model))
						return Optional.of(car);
				}
				case Camel camel -> {}
			}
		}
		return Optional.empty();
	}

	@Override
	public String toString() {
		return rentables.stream().map(Rentable::toString).collect(Collectors.joining("\n"));
	}
}
