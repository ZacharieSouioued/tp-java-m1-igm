package fr.umlv.movie;

import lombok.NonNull;

public record ActorMovieCount(@NonNull String actor, Long movieCount) implements Comparable<ActorMovieCount>{


	@Override
	public int compareTo(ActorMovieCount o) {
		return Long.compare(this.movieCount, o.movieCount);
	}
}
