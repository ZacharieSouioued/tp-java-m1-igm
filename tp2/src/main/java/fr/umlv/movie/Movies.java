package fr.umlv.movie;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Movies {

	public static List<Movie> movies(Path path) throws IOException {
		try(var file = Files.lines(path)) {
			return file.map(line -> {
					var str = line.split(";", 2);
					return new Movie(str[0], List.of(str[1].split(";")));
				})
				.collect(Collectors.toUnmodifiableList());
		}
	}

	public static Map<String, Movie> movieMap(List<Movie> movies) {
		return movies.stream()
				.collect(Collectors.toUnmodifiableMap(Movie::title, Function.identity()));
	}

	public static Long numberOfUniqueActors(List<Movie> movies) {
		return movies.stream()
				.map(Movie::actors)
				.flatMap(List::stream)
				.distinct()
				.count();
	}

	public static Map<String, Long> numberOfMoviesByActor(List<Movie> movies) {
		return movies.stream()
				.map(Movie::actors)
				.flatMap(List::stream)
				.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
	}

	public static Optional<ActorMovieCount> actorInMostMovies(Map<String, Long> actors) {
		return actors.entrySet().stream()
				.map(actor -> new ActorMovieCount(actor.getKey(), actor.getValue()))
				.collect(Collectors.maxBy(ActorMovieCount::compareTo)); // or max(ActorMovieCount::compareTo);
	}
}
