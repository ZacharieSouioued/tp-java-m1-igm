package fr.umlv.movie;

import lombok.NonNull;

import java.util.List;

public record Movie(@NonNull String title, @NonNull List<String> actors) {

	public Movie(@NonNull String title, @NonNull List<String> actors){
		this.title = title;
		this.actors = List.of(actors.toArray(new String[]{}));
	}

}
