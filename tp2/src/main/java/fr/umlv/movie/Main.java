package fr.umlv.movie;

import java.io.IOException;
import java.nio.file.*;

public class Main {

	public static void main(String[] args) throws IOException {
		var path = Path.of(args[1]);
		try(var file = Files.lines(path)) {
			System.out.println(file.count());
		}
	}
}
